import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Login from "./components/pages/LoginPage";
import Register from "./components/pages/RegisterPage";
import ListPurchase from "./components/pages/ListPurchasePage";
import ListItem from "./components/pages/ListItemPage";
import DetailPurchase from "./components/DetailPurchase";
import AddPurchase from "./components/AddPurchase";
import AddItem from "./components/AddItem";
import DeletePurchase from "./components/DeletePurchase";
import UpdatePurchase from "./components/UpdatePurchase";
import UpdateItem from "./components/UpdateItem";
import CheckLogin from "./components/CheckLogin";
import React from "react";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Redirect to="/login" />
        </Route>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <React.Fragment>
          <CheckLogin />
          <Route path="/purchase/:id" component={DetailPurchase} />
          <Route path="/deletepurchase/:id" component={DeletePurchase} />
          <Route path="/updatepurchase/:id" component={UpdatePurchase} />
          <Route path="/updateitem/:id" component={UpdateItem} />
          <Route path="/addpurchase" component={AddPurchase} />
          <Route path="/additem" component={AddItem} />
          <Route path="/listpurchase" component={ListPurchase} />
          <Route path="/listitem" component={ListItem} />
        </React.Fragment>
      </Switch>
    </Router>
  );
}

export default App;
