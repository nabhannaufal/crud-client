import React, { useState, useEffect } from "react";
import {
  Input,
  Form,
  FormGroup,
  Label,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router";

const UpdateItem = () => {
  const history = useHistory();
  const { id } = useParams();
  const [token] = useState(localStorage.getItem("token") || null);
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [stock, setStock] = useState("");

  useEffect(() => {
    const getData = async () => {
      const result = await axios.get(`http://localhost:8000/api/item/${id}`, {
        headers: { authorization: token },
      });
      setName(result.data.message.name);
      setPrice(result.data.message.price);
      setStock(result.data.message.stock);
    };
    getData();
  }, [token, id]);

  const formatData = {
    name: name,
    price: price,
    stock: stock,
  };

  const handleUpdate = (e) => {
    e.preventDefault();
    axios
      .put(`http://localhost:8000/api/item/${id}`, formatData, {
        headers: { authorization: token },
      })
      .then(function (response) {
        console.log(response);
        history.push("/listitem");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  return (
    <div className="morningline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex justify-content-between mb-2">
              <div
                className="title text-success align-self-center text-center"
                style={{ width: 300 }}
              >
                <h5>
                  <b>Update Item</b>
                </h5>
              </div>
            </div>
            <Form onSubmit={(e) => handleUpdate(e)}>
              <FormGroup className="mb-2">
                <Label>Name:</Label>
                <Input
                  onChange={(e) => setName(e.target.value)}
                  value={name}
                  type="text"
                  required
                  placeholder="name"
                />
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Price:</Label>
                <Input
                  onChange={(e) => setPrice(e.target.value)}
                  type="number"
                  value={price}
                  required
                  placeholder="Price"
                />
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Stock:</Label>
                <Input
                  onChange={(e) => setStock(e.target.value)}
                  type="number"
                  required
                  value={stock}
                  placeholder="Stock"
                />
              </FormGroup>
              <div className="d-flex column justify-content-between mt-3">
                <Button color="danger" href="/listitem">
                  Cancel <i className="fas fa-times-circle"></i>
                </Button>
                <Button type="submit" color="primary">
                  Update <i className="fas fa-marker"></i>
                </Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default UpdateItem;
