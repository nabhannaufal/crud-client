import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import { Container, Input, Form, Button } from "reactstrap";

const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const history = useHistory();

  const handleRegistration = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/auth/register", {
        username,
        email,
        password,
      })
      .then(function (response) {
        console.log("response: ", response);
        history.push("/login");
        alert("register success!");
      })
      .catch(function (error) {
        console.log(error);
        alert("Error while retrieving data or username is exist");
      });
  };
  return (
    <div className="login-section ">
      <Container className="container-col">
        <div className="white-box-login">
          <div className="title mb-4">
            <h2 className="text-pink text-center">Sign Up!</h2>
          </div>
          <Form className="login-form" onSubmit={(e) => handleRegistration(e)}>
            <Input
              className="input-login"
              type="username"
              placeholder="Username"
              onChange={(e) => setUsername(e.target.value)}
              required
            />
            <Input
              className="input-login"
              type="email"
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <Input
              className="input-login"
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            <div>
              <Button type="submit" color="danger" className="btn-login">
                Submit
              </Button>
            </div>
            <div className="text-center mt-2">
              Have an account? <Link to="/login">Login</Link>
            </div>
          </Form>
        </div>
      </Container>
    </div>
  );
};
export default Register;
