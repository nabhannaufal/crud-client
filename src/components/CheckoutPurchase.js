import React, { useState, useEffect } from "react";
import {
  Input,
  Form,
  FormGroup,
  Label,
  Button,
  Container,
  Row,
  Col,
  Table,
} from "reactstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";

const CheckoutPurchase = () => {
  const history = useHistory();
  const [token] = useState(localStorage.getItem("token") || null);
  const [data, setData] = useState();
  const [itemId, setItemId] = useState("1");
  const [quantity, setQuantity] = useState("");
  const [date, setDate] = useState("");
  const [namaItem, setNamaItem] = useState("");
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get("http://localhost:8000/api/item", {
        headers: { authorization: token },
      });
      setData(result.data);
    };
    fetchData();
  }, [token, history]);
  console.log(data);

  const formatData = {
    item_id: itemId,
    quantity: quantity,
    date: date,
  };

  const handleAddPurchase = (e) => {
    localStorage.setItem("item_id", setItemId());
    localStorage.setItem("nama_item");
    localStorage.setItem("quantity", setQuantity());
    localStorage.setItem("date", setDate());
  };

  const getListItems = () => {
    if (data !== undefined) {
      return (
        <>
          {data.map((item, index) => (
            <option key={index} value={item.id}>
              {item.name}
            </option>
          ))}
        </>
      );
    } else {
      return <option>Loading data</option>;
    }
  };
  return (
    <div className="nightline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex mb-2 justify-content-center">
              <div className="title text-pink text-center">
                <h4>
                  <b>Are you Sure? </b>
                </h4>
              </div>
            </div>
            <Table striped>
              <tbody>
                <tr>
                  <td>Item ID</td>
                  <td>:</td>
                  <td>{itemId}</td>
                </tr>
                <tr>
                  <td>Item name</td>
                  <td>:</td>
                  <td>{itemId}</td>
                </tr>
                <tr>
                  <td>Quantity</td>
                  <td>:</td>
                  <td>{quantity}</td>
                </tr>
                <tr>
                  <td>Total Price</td>
                  <td>:</td>
                  <td>{quantity}</td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>:</td>
                  <td>{date}</td>
                </tr>
              </tbody>
            </Table>
            <div className="d-flex row justify-content-center mb-2">
              <div className="title text-center text-muted">
                <h6>Checkout Now!</h6>
              </div>
              <div className="mt-3 justify-content-between d-flex">
                <Button
                  color="success"
                  href="/listpurchase"
                  className="btn-action"
                >
                  Cancel <i className="fas fa-times-circle"></i>
                </Button>
                <Button
                  onClick={handleAddPurchase}
                  color="danger"
                  className="btn-action"
                >
                  Delete <i className="fas fa-trash-alt"></i>
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default CheckoutPurchase;
