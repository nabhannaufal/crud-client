import React, { useState, useEffect } from "react";
import {
  Input,
  Form,
  FormGroup,
  Label,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";

const AddPurchase = () => {
  const history = useHistory();
  const [token] = useState(localStorage.getItem("token") || null);
  const [data, setData] = useState();
  const [detail, setDetail] = useState();
  const [itemId, setItemId] = useState("1");
  const [quantity, setQuantity] = useState("");
  const [date, setDate] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get("http://localhost:8000/api/item", {
        headers: { authorization: token },
      });
      setData(result.data);
    };
    fetchData();
  }, [token, history, itemId]);
  console.log(data);

  const formatData = {
    item_id: itemId,
    quantity: quantity,
    date: date,
  };

  const handleAddPurchase = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/purchase/checkout", formatData, {
        headers: { authorization: token },
      })
      .then(function (response) {
        console.log(response);
        history.push("/listpurchase");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const getListItems = () => {
    if (data !== undefined) {
      return (
        <>
          {data.map((item, index) => (
            <option key={index} value={item.id}>
              {item.name}
            </option>
          ))}
        </>
      );
    } else {
      return <option>Loading data</option>;
    }
  };
  return (
    <div className="redline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex justify-content-between mb-2">
              <div
                className="title text-oren align-self-center text-center"
                style={{ width: 300 }}
              >
                <h5>
                  <b>Add Purchase</b>
                </h5>
              </div>
            </div>
            <Form onSubmit={(e) => handleAddPurchase(e)}>
              <FormGroup className="mb-2">
                <Label>Select Item:</Label>
                <Input
                  onChange={(e) => setItemId(e.target.value)}
                  type="select"
                  required
                >
                  {getListItems()}
                </Input>
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Quantity :</Label>
                <Input
                  onChange={(e) => setQuantity(e.target.value)}
                  type="number"
                  required
                  placeholder="Quantity"
                />
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Date :</Label>
                <Input
                  onChange={(e) => setDate(e.target.value)}
                  type="date"
                  required
                  placeholder="Date"
                />
              </FormGroup>
              <div className="d-flex column justify-content-between mt-3">
                <Button color="danger" href="/listpurchase">
                  Cancel <i className="fas fa-times-circle"></i>
                </Button>
                <Button type="submit" color="success">
                  Checkout <i className="fas fa-check-circle"></i>
                </Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default AddPurchase;
