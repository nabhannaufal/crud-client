import React, { useState, useEffect } from "react";
import {
  Input,
  Form,
  FormGroup,
  Label,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router";

const UpdatePurchase = () => {
  const history = useHistory();
  const { id } = useParams();
  const [token] = useState(localStorage.getItem("token") || null);
  const [itemId, setItemId] = useState("1");
  const [quantity, setQuantity] = useState("");
  const [date, setDate] = useState("");

  useEffect(() => {
    const getData = async () => {
      const result = await axios.get(
        `http://localhost:8000/api/purchase/${id}`,
        {
          headers: { authorization: token },
        }
      );
      setItemId(result.data.message.item_id);
      setQuantity(result.data.message.quantity);
      setDate(result.data.message.date);
    };
    getData();
  }, [token, id]);

  const formatData = {
    item_id: itemId,
    quantity: quantity,
    date: date,
  };

  const handleUpdate = (e) => {
    e.preventDefault();
    axios
      .put(`http://localhost:8000/api/purchase/${id}`, formatData, {
        headers: { authorization: token },
      })
      .then(function (response) {
        console.log(response);
        history.push("/listpurchase");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  return (
    <div className="morningline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex justify-content-between mb-2">
              <div
                className="title text-success align-self-center text-center"
                style={{ width: 300 }}
              >
                <h5>
                  <b>Update Purchase</b>
                </h5>
              </div>
            </div>
            <Form onSubmit={(e) => handleUpdate(e)}>
              <FormGroup className="mb-2">
                <Label>Item Id</Label>
                <Input
                  onChange={(e) => setQuantity(e.target.value)}
                  type="number"
                  required
                  placeholder="Quantity"
                  value={itemId}
                />
              </FormGroup>

              <FormGroup className="mb-2">
                <Label>Quantity :</Label>
                <Input
                  onChange={(e) => setQuantity(e.target.value)}
                  type="number"
                  required
                  placeholder="Quantity"
                  value={quantity}
                />
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Date :</Label>
                <Input
                  onChange={(e) => setDate(e.target.value)}
                  type="date"
                  required
                  value={date}
                  placeholder="Date"
                />
              </FormGroup>
              <div className="d-flex column justify-content-between mt-3">
                <Button color="danger" href="/listpurchase">
                  Cancel <i className="fas fa-times-circle"></i>
                </Button>
                <Button type="submit" color="primary">
                  Update <i className="fas fa-marker"></i>
                </Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default UpdatePurchase;
