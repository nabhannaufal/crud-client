import { useEffect } from "react";
import { useHistory } from "react-router";

const CheckLogin = () => {
  const history = useHistory();
  useEffect(() => {
    const login = localStorage.getItem("login");
    if (!login) {
      return history.push("/login");
    }
  }, [history]);
  return null;
};

export default CheckLogin;
