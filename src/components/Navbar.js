import {
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import React, { useState } from "react";
import { useHistory } from "react-router";

const Navbar = (props) => {
  const history = useHistory();
  const [username] = useState(localStorage.getItem("username") || null);
  const [role] = useState(localStorage.getItem("role") || null);

  const handleLogout = () => {
    localStorage.removeItem("login");
    localStorage.removeItem("username");
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    history.push("/login");
  };
  const chooseActive = (isActive) => {
    switch (isActive) {
      case "admin":
        return (
          <>
            <NavItem>
              <NavLink href="/listitem">Items</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/listpurchase">Purchased</NavLink>
            </NavItem>
          </>
        );
      case "regular":
        return (
          <>
            <NavItem>
              <NavLink href="/listpurchase">Purchased</NavLink>
            </NavItem>
          </>
        );
      default:
        return (
          <>
            <NavItem>
              <NavLink href="/listitem">Items</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/listpurchase" active>
                Purchased
              </NavLink>
            </NavItem>
          </>
        );
    }
  };

  return (
    <Nav tabs justified>
      <NavbarBrand>
        <i className="fas fa-home"></i> Dashboard
      </NavbarBrand>
      {chooseActive(props.tab)}
      <UncontrolledDropdown>
        <DropdownToggle nav caret>
          {username}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={handleLogout}>
            <h6>
              <i className="fas fa-power-off"></i> Logout
            </h6>
          </DropdownItem>
          <DropdownItem onClick={handleLogout}>
            <h6>role:{role}</h6>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    </Nav>
  );
};

export default Navbar;
