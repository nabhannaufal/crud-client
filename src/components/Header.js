import { Row } from "reactstrap";
const Header = () => {
  return (
    <Row className="black-box-list mt-5 text-center">
      <h2>
        <i className="fas fa-file-code"></i> React App
      </h2>
    </Row>
  );
};

export default Header;
