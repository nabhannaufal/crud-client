import React, { useState } from "react";
import {
  Input,
  Form,
  FormGroup,
  Label,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";

const AddItem = () => {
  const history = useHistory();
  const [token] = useState(localStorage.getItem("token") || null);
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [stock, setStock] = useState("");

  const formatData = {
    name: name,
    price: price,
    stock: stock,
  };

  const handleAdd = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/item/add", formatData, {
        headers: { authorization: token },
      })
      .then(function (response) {
        console.log(response);
        history.push("/listitem");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  return (
    <div className="redline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex justify-content-between mb-2">
              <div
                className="title text-oren align-self-center text-center"
                style={{ width: 300 }}
              >
                <h5>
                  <b>Add Item</b>
                </h5>
              </div>
            </div>
            <Form onSubmit={(e) => handleAdd(e)}>
              <FormGroup className="mb-2">
                <Label>Name:</Label>
                <Input
                  onChange={(e) => setName(e.target.value)}
                  type="text"
                  required
                  placeholder="name"
                />
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Price:</Label>
                <Input
                  onChange={(e) => setPrice(e.target.value)}
                  type="number"
                  required
                  placeholder="Price"
                />
              </FormGroup>
              <FormGroup className="mb-2">
                <Label>Stock:</Label>
                <Input
                  onChange={(e) => setStock(e.target.value)}
                  type="number"
                  required
                  placeholder="Stock"
                />
              </FormGroup>

              <div className="d-flex column justify-content-between mt-3">
                <Button color="danger" href="/listitem">
                  Cancel <i className="fas fa-times-circle"></i>
                </Button>
                <Button type="submit" color="success">
                  Submit <i className="fas fa-check-circle"></i>
                </Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default AddItem;
