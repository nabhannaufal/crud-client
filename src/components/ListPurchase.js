import React, { useState, useEffect } from "react";
import axios from "axios";
import { Button, Table, Container, Col, Row } from "reactstrap";
import Navbar from "./Navbar";
import Header from "./Header";

const ListPurchase = () => {
  const [token] = useState(localStorage.getItem("token") || null);
  const [data, setData] = useState();
  const [role, setRole] = useState(localStorage.getItem("role"));

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get("http://localhost:8000/api/purchase", {
        headers: { authorization: token },
      });
      setData(result.data);
    };

    fetchData();
  }, [token]);

  const getData = () => {
    if (data !== undefined) {
      return (
        <>
          {data.map((item, index) => (
            <tr key={index} className="text-center">
              <th scope="row">{index + 1}</th>
              <td>{item.item_id}</td>
              <td>{item.quantity}</td>
              <td>{item.item.name}</td>
              <td>{item.item.price}</td>
              <td>{item.date} </td>
              <td>
                <a href={"/purchase/" + item.id} className="fas fa-eye">
                  {""}
                </a>
                <a
                  href={"/updatepurchase/" + item.id}
                  className="fas fa-edit text-success edit-icon"
                >
                  {" "}
                </a>
                <a
                  href={"/deletepurchase/" + item.id}
                  className="fas fa-trash-alt text-danger"
                >
                  {" "}
                </a>
              </td>
            </tr>
          ))}
        </>
      );
    } else {
      return (
        <tr>
          <th>data loading</th>
        </tr>
      );
    }
  };

  return (
    <div className="skyline" style={{ height: "120vh" }}>
      <Container className="cont">
        <Header />
        <Row className="white-box-list mt-4">
          <Col>
            <Navbar tab={role} />
            <div className="d-flex justify-content-between mb-2 mt-3">
              <Button color="primary" style={{ opacity: 0, cursor: "default" }}>
                <i className="fas fa-plus"></i>
              </Button>
              <div className="title text-pink align-self-center">
                <h4>
                  <b>PURCHASED ITEMS</b>
                </h4>
              </div>
              <Button
                href="/addpurchase"
                color="primary"
                style={{ borderRadius: 30 }}
              >
                <i className="fas fa-plus"></i>
              </Button>
            </div>
            <Table hover responsive>
              <thead>
                <tr className="text-center">
                  <th>#</th>
                  <th>Item ID</th>
                  <th>Quantity</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{getData()}</tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ListPurchase;
