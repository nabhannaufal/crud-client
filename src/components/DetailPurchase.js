import { useParams } from "react-router";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Button, Table, Container, Row, Col } from "reactstrap";

const DetailPurchase = () => {
  const { id } = useParams();
  const [token] = useState(localStorage.getItem("token") || null);
  const [itemId, setItemId] = useState("");
  const [quantity, setQuantity] = useState("");
  const [date, setDate] = useState("");
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [stock, setStock] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `http://localhost:8000/api/purchase/${id}`,
        {
          headers: { authorization: token },
        }
      );
      setItemId(result.data.message.item_id);
      setQuantity(result.data.message.quantity);
      setDate(result.data.message.date);
      setName(result.data.message.item.name);
      setPrice(result.data.message.item.price);
      setStock(result.data.message.item.stock);
    };

    fetchData();
  }, [token, id]);

  return (
    <div className="limeline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex justify-content-between mb-2">
              <div
                className="title text-pink align-self-center text-center"
                style={{ width: 300 }}
              >
                <h4>
                  <b>Detail Purchase</b>
                </h4>
              </div>
            </div>
            <Table striped>
              <tbody>
                <tr>
                  <td>Puchase ID</td>
                  <td>:</td>
                  <td>{id}</td>
                </tr>
                <tr>
                  <td>Item ID</td>
                  <td>:</td>
                  <td>{itemId}</td>
                </tr>
                <tr>
                  <td>Quantity</td>
                  <td>:</td>
                  <td>{quantity}</td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>:</td>
                  <td>{date}</td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td>:</td>
                  <td>{name}</td>
                </tr>
                <tr>
                  <td>Price</td>
                  <td>:</td>
                  <td>{price}</td>
                </tr>
                <tr>
                  <td>Stock</td>
                  <td>:</td>
                  <td>{stock}</td>
                </tr>
              </tbody>
            </Table>
            <div className="width-btn">
              <Button color="danger" href="/listpurchase">
                <i className="fas fa-arrow-circle-left"></i> Back
              </Button>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default DetailPurchase;
