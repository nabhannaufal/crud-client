import axios from "axios";
import { useState, useEffect } from "react";
import { Button, Table, Container, Row, Col } from "reactstrap";
import { useHistory } from "react-router";
import { useParams } from "react-router";

const DeletePurchase = () => {
  const { id } = useParams();
  const [token] = useState(localStorage.getItem("token") || null);
  const [itemId, setItemId] = useState("");
  const [quantity, setQuantity] = useState("");
  const [date, setDate] = useState("");
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `http://localhost:8000/api/purchase/${id}`,
        {
          headers: { authorization: token },
        }
      );
      setItemId(result.data.message.item_id);
      setQuantity(result.data.message.quantity);
      setDate(result.data.message.date);
    };

    fetchData();
  }, [token, id]);

  const handleDelete = () => {
    axios
      .delete(`http://localhost:8000/api/purchase/${id}`, {
        headers: { authorization: token },
      })
      .then(function (response) {
        console.log(response);
        history.push("/listpurchase");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  return (
    <div className="nightline">
      <Container className="cont cont-center">
        <Row className="white-box">
          <Col>
            <div className="d-flex mb-2 justify-content-center">
              <div className="title text-pink text-center">
                <h4>
                  <b>Are you Sure? </b>
                </h4>
              </div>
            </div>
            <Table striped>
              <tbody>
                <tr>
                  <td>Puchase ID</td>
                  <td>:</td>
                  <td>{id}</td>
                </tr>
                <tr>
                  <td>Item ID</td>
                  <td>:</td>
                  <td>{itemId}</td>
                </tr>
                <tr>
                  <td>Quantity</td>
                  <td>:</td>
                  <td>{quantity}</td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>:</td>
                  <td>{date}</td>
                </tr>
              </tbody>
            </Table>
            <div className="d-flex row justify-content-center mb-2">
              <div className="title text-center text-muted">
                <h6>This data will be remove!</h6>
              </div>
              <div className="mt-3 justify-content-between d-flex">
                <Button
                  color="success"
                  href="/listpurchase"
                  className="btn-action"
                >
                  Cancel <i className="fas fa-times-circle"></i>
                </Button>
                <Button
                  onClick={handleDelete}
                  color="danger"
                  className="btn-action"
                >
                  Delete <i className="fas fa-trash-alt"></i>
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default DeletePurchase;
