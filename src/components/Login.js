import React, { useState } from "react";
import { Container, Input, Form, Button } from "reactstrap";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/auth/login", {
        username: username,
        password: password,
      })
      .then(function (response) {
        localStorage.setItem("token", response.data.accessToken);
        localStorage.setItem("username", response.data.username);
        localStorage.setItem("login", true);
        localStorage.setItem("role", response.data.role);
        console.log("role", response.data.role);
        history.push("/listpurchase");
      })
      .catch(function (error) {
        console.log(error);
        alert("username or password incorrect");
      });
  };
  return (
    <div className="login-section ">
      <Container className="container-col">
        <div className="white-box-login">
          <div className="title mb-4">
            <h2 className="text-info text-center">Welcome Back!</h2>
          </div>
          <Form onSubmit={(e) => handleSubmit(e)} className="login-form">
            <Input
              className="input-login"
              type="username"
              placeholder="username"
              onChange={(e) => setUsername(e.target.value)}
              required
            />
            <Input
              className="input-login"
              type="password"
              placeholder="password"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            <div>
              <Button
                color="info"
                type="submit"
                className="btn-login text-white"
              >
                Submit
              </Button>
            </div>
            <div className="text-center mt-2">
              Don{"'"}t Have an account? <Link to="/register">SignUp</Link>
            </div>
          </Form>
        </div>
      </Container>
    </div>
  );
};

export default Login;
